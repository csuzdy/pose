import { Pose } from "@tensorflow-models/posenet";

export default interface PoseDetectionResult {
  pose: Pose;
}